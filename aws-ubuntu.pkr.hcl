packer {
  required_plugins {
    amazon = {
      version = ">= 0.0.2"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

source "amazon-ebs" "ubuntu" {
  ami_name      = "aws-test-ubuntu"
  instance_type = "t2.micro"
  region        = "eu-west-3"
  source_ami_filter {
    filters = {
      name                = "ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-20211021"
      root-device-type    = "ebs"
      virtualization-type = "hvm"
    }
    most_recent = true
    owners      = ["099720109477"]
  }
  vpc_filter {
    filters = {
      "isDefault" : "true"
    }
  }
  subnet_id    = "${subnet_id}"
  vpc_id = "${vpc_id}"
  ssh_username = "ubuntu"
}

build {
  name = "aws-test-ubuntu"
  sources = [
    "source.amazon-ebs.ubuntu"
  ]

  provisioner "shell" {
    inline = [
      "echo Add User Ansible",
      "sudo useradd ansible",
      "sudo apt-get update",
      "sudo apt-get install -y nano",
    ]
  }
}