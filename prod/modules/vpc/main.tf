resource "aws_vpc" "vpc_packer" {
  instance_tenancy     = "default"
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    "Name" = "${var.prefix_name}-vpc"
  }
}

#Public subnet
resource "aws_subnet" "main-public" {
  vpc_id                  = aws_vpc.vpc_packer.id
  cidr_block              = var.public_subnet_cidr
  map_public_ip_on_launch = true
  availability_zone       = "${var.aws_region}a"
  tags = {
    Name = "${var.prefix_name}-public"
  }
}

# Internet GW
resource "aws_internet_gateway" "main-gw" {
  vpc_id = aws_vpc.vpc_packer.id

  tags = {
    Name = "${var.prefix_name}-ig"
  }
}

# Public route tables
resource "aws_route_table" "main-public" {
  vpc_id = aws_vpc.vpc_packer.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main-gw.id
  }

  tags = {
    Name = "${var.prefix_name}-public-rt"
  }
}

# Route associations public
resource "aws_route_table_association" "main-public" {
  subnet_id      = aws_subnet.main-public.id
  route_table_id = aws_route_table.main-public.id
}

# Elastic IP for NAT gw
resource "aws_eip" "nat" {
  vpc = true

  tags = {
    Name = "${var.prefix_name}-eip"
  }
}

# Nat gw
resource "aws_nat_gateway" "nat-gw" {
  allocation_id = aws_eip.nat.id
  subnet_id     = aws_subnet.main-public.id
  depends_on    = [aws_internet_gateway.main-gw]
}