output "vpc_id" {
  value = aws_vpc.vpc_packer.id
}

output "public_subnet_id" {
  value = aws_subnet.main-public.id
}